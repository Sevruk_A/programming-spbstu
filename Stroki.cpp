//
//  main.cpp
//  Задание 5
//
//  Created by Александр Севрук on 17.12.14.
//  Copyright (c) 2014 Александр Севрук. All rights reserved.
//

#include <iostream>
#include <string.h>

int main()
{
    char s[50];
    
    std::cin.getline(s, 50);
    
    int p=0, k, i=50, l=i-strlen(s);
    
    for (k = 0; k < strlen(s); k++)
        
        p+=(s[k]==' ')?1:0;
    
    p=l/p;
    
    for (k = 0; k < strlen(s); k++) {
        
        if (s[k]!=' ') {
            
            std::cout << s[k];
        
        }
        else {
            for (i = 0; i < p; i++) {
                std::cout << ' ';
            
            }
        }
    }
    
    std::cout<<'\n';
    
    return 0;
}

