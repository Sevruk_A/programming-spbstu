#include "stdafx.h"

game::game() {
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			square[i][j] = C_NONE;
}

game::~game()
{
}

bool game::whoWalks()
{
	return (move == M_X) ? true : false;
}

bool set(int x, int y)
{
    if (makeMove(x, y) == false)
    {
        std::cout << "¬˚ ‚‚ÂÎË ÌÂ‚ÂÌÛ˛ ˇ˜ÂÈÍÛ ËÎË ˝Ú‡ ˇ˜ÂÈÍ‡ Á‡ÌˇÚ‡, ‚‚Â‰ËÚÂ ‰Û„Û˛:" << std::endl;
        return false;
    }
    return true;
}


bool game::makeMove(int x, int y)
{
	if (x < 0 || x > 3 || y < 0 || y > 3 || square[x][y] != C_NONE)
	{
		return false;
	}
	else
	{
		square[x][y] = (move == M_X) ? C_X : C_TOE;
		if (checkWin() == false)
			move = (move == M_X) ? M_TOE : M_X;
		return true;
	}
}

bool game::checkWin()
{
	for (int i = 0; i < 3; i++)
	{
		if (square[i][0] != C_NONE && (square[i][0] == square[i][1] && square[i][0] == square[i][1] == square[i][2]))
			win = (square[i][0] == C_X) ? M_X : M_TOE;
		else if (square[0][i] != C_NONE && (square[0][i] == square[1][i] && square[0][i] == square[1][i] == square[2][i]))
			win = (square[0][i] == C_X) ? M_X : M_TOE;
	}
	if (win == M_NONE)
	{
		if (square[0][0] != C_NONE && (square[0][0] == square[1][1] && square[1][1] == square[2][2]))
			win = (square[0][0] == C_X) ? M_X : M_TOE;
		else if (square[0][2] != C_NONE && (square[0][2] == square[1][1] && square[1][1] == square[2][0]))
			win = (square[0][2] == C_X) ? M_X : M_TOE;
	}
	return (win != M_NONE) ? true : false;
}

bool game::isOver()
{
	if (win == M_NONE)
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (square[i][j] == C_NONE)
					return false;
	return true;
}

int game::result()
{
    if (win == M_NONE)	return 0;
	else				return ((win == M_X) ? 1 : 2);
}

std::ostream &operator<< (std::ostream & os, game &g)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (square[i][j] == C_X)
				os << "X\t";
			else if (square[i][j] == C_TOE)
				os << "O\t";
			else
				os << "\t";
		}
		os << std::endl;
	}
}