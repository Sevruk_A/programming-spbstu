//
//  main.cpp
//  Задание 4
//
//  by Александр Севрук on 11.12.14.
//  Copyright (c) 2014 Александр Севрук. All rights reserved.
//

#include <iostream>
using namespace std;

int main () {
    
    int n = 3;
    int i, j, m, r, g;
    int mas[n][n];
    
    for (i = 0; i < n; ++i)
    {
        for (j = 0; j < n; ++j)
        {
            cin >> mas[i][j];
        }
    }
    
    g = 0;
    
    for (j = 0; j<=n-2; j++) {
        i = 0;
        while (mas[i][j] != 0) {
            i = i + 1;
        }
        if (i != j)
            for (r = 0; r<=n-1; r++) {
             m = mas[i][r];
             mas[i][r] = mas[g][r];
             mas[g][r] = m;
            
        }
        g = g+1;
    }
    
    for (i = 0; i < n; ++i)
    {
        for (j = 0; j < n; ++j)
        {
            cout << mas[i][j];
        }
    }
    
    return 0;
}