

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <locale>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::locale;
using std::string;


int main()
{
	locale::global(locale(""));

	unsigned a[9] = { 1000, 200, 100, 50, 10, 9, 5, 4, 1 };
	string r[9] = { "M", "D", "C", "L", "X", "IX", "V", "IV", "I" };
	unsigned n;
	string s;
	std::ifstream ifs("d:\\in.txt");
	std::ofstream ofs("d:\\out.txt");
	while (!ifs.eof())
	{
		s.clear();
		ifs >> n;
		cout << n;
		for (unsigned i = 0; i < 9; ++i)
		{
			unsigned c = n / a[i];
			for (unsigned j = 0; j < c; ++j) s += r[i];
			n %= a[i];
		}
		ofs << s << endl;
		cout << " = " << s << endl;
	}

	ifs.close();
	ofs.close();

	cout << endl;
	system("pause");
	return 0;
}