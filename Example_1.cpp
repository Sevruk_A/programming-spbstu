
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <locale>
#include <vector>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::locale;
using std::string;


int main()
{
	locale::global(locale(""));

	int n;
	std::vector<int> F;
	F.push_back(1);
	F.push_back(2);
	string r;

	std::ofstream ofs("d://out.txt");
	std::ifstream ifs("d://in.txt");
	if (!ifs)
	{
		cout << "Ошибка при открытии файла." << endl;
	}
	else
	{
		while (true)
		{
			ifs >> n;
			if (!ifs.good()) break;
			r.clear();
			while (*(F.rbegin()) < n) F.push_back(*(F.rbegin()) + *(F.rbegin() + 1));
			std::vector<int>::reverse_iterator i = F.rbegin();
			while (*i > n) ++i;
			for (; i != F.rend(); ++i)
			{
				int ttt = *i;
				if (n - *i >= 0)
				{
					n -= *i;
					r += '1';
				}
				else r += '0';
			}
			ofs << r << endl;
			cout << r << endl;
		}

	}

	ofs.close();

	cout << endl;
	system("pause");
	return 0;
}