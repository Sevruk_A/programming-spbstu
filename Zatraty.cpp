//
//  main.cpp
//  Задание 2
//
//  Created by Александр Севрук on 10.12.14.
//  Copyright (c) 2014 Александр Севрук. All rights reserved.
//

#include <iostream>
#include <cmath>
using namespace std;

int main() {
    double v1, v2, p1, s, t,  p;
    scanf("%lf %lf %lf %lf %lf", &v1, &v2, &s, &p1, &t);
    double T = (s - t * v2) / (v1 - v2);
    if( s / v2 < t)
        printf("0");
    else if( v1 <= v2 ) {
        if( v2 * t < s )
            printf("No time");
        else
            printf("0");
    }
    else if(T <= t) {
        p=(T * v1) * p1;
        printf("%lf", p);
    }
    else
        printf("No time");
    
    return 0;
}

