//
//  main.cpp
//  Задание 1
//
//  Created by Александр Севрук on 10.12.14.
//  Copyright (c) 2014 Александр Севрук. All rights reserved.
//


#include <iostream>
using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    const double pi = 3.14;
    double grad;
    double min;
    double sec;
    cout<<"Введите градусы, минуты и секунды: "<<endl;
    cin>>grad>>min>>sec;
    double rad=(grad+min/60.+sec/3600.)/180.*pi;
    cout<<"Полученный угол в радианах"<<rad<<endl;

    return 0;
}