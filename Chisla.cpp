//
//  main.cpp
//  Задание 3
//
//  Created by Александр Севрук on 10.12.14.
//  Copyright (c) 2014 Александр Севрук. All rights reserved.
//

#include <iostream>
using namespace std;

int main() {
    
    int M, N, n1, m1, max;
    cin >> M >> N;
    
    n1 = N%10;
    m1 = M%10;
    
    if (n1 > m1) {
        max = n1;
    } else {
        max = m1;
    }
    
    n1 = (N%100)/10;
    m1 = (M%100)/10;
    
    if (n1 > m1) {
        max = max + n1*10;
    } else {
        max = max + m1*10;
    }
    
    n1 = (N%1000)/100;
    m1 = (M%1000)/100;
    
    if (n1 > m1) {
        max = max + n1*100;
    } else {
        max = max + m1*100;
    }
    
    n1 = N/1000;
    m1 = M/1000;
    
    if (n1 > m1) {
        max = max + n1*1000;
    } else {
        max = max + m1*1000;
    }
    
    cout << max;
    
    return 0;
}
